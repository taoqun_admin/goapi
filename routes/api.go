// Package routes 注册路由
package routes

import (
	"github.com/gin-gonic/gin"
	"goapi/app/http/controllers/api/v1/auth"
	"goapi/app/http/middlewares"
)

// RegisterAPIRoutes 注册网页相关路由
func RegisterAPIRoutes(r *gin.Engine) {

	// 测试一个 v1 的路由组，我们所有的 v1 版本的路由都将存放到这里
	v1 := r.Group("/v1")
	{
		v1.Use(middlewares.LimitIP("200-H"))
		{
			authGroup := v1.Group("/auth")
			{
				suc := new(auth.SignupController)
				// 判断手机是否已注册
				authGroup.POST("/signup/phone/exist", suc.IsPhoneExist)
				// 判断 Email 是否已注册
				authGroup.POST("/signup/email/exist", suc.IsEmailExist)

				// 发送验证码
				vcc := new(auth.VerifyCodeController)
				// 图片验证码，需要加限流
				authGroup.POST("/verify-codes/captcha", vcc.ShowCaptcha)
				authGroup.POST("/verify-codes/phone", vcc.SendUsingPhone)
				authGroup.POST("/verify-codes/email", vcc.SendUsingEmail)
				authGroup.POST("/signup/using-phone", suc.SignupUsingPhone)
				authGroup.POST("/signup/using-email", suc.SignupUsingEmail)

				lgc := new(auth.LoginController)
				// 使用手机号，短信验证码进行登录
				authGroup.POST("/login/using-phone", lgc.LoginByPhone)
				// 支持手机号，Email 和 用户名
				authGroup.POST("/login/using-password", lgc.LoginByPassword)
				authGroup.POST("/login/refresh-token", lgc.RefreshToken)

				// 重置密码
				pwc := new(auth.PasswordController)
				authGroup.POST("/password-reset/using-phone", pwc.ResetByPhone)
				authGroup.POST("/password-reset/using-email", pwc.ResetByEmail)

			}
		}
	}
}
